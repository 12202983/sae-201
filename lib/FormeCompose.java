import java.util.ArrayList;
import ardoise.*;

public abstract class FormeCompose extends Forme {

  public String nom;
  private ArrayList<Forme> formesComposees;

  public FormeCompose(String nomForme) {
    super(nomForme);
    this.formesComposees = new ArrayList<>();
  }

  public void ajouterForme(Forme forme) {
    formesComposees.add(forme);
  }

  public void retirerForme(Forme forme) {
    formesComposees.remove(forme);
  }

  public void deplacer(int deplacementX, int deplacementY) {
    for (Forme forme : formesComposees) {
      forme.deplacer(deplacementX, deplacementY);
    }
  }

  public ArrayList<Segment> dessiner() {
    ArrayList<Segment> segments = new ArrayList<>();
    for (Forme forme : formesComposees) {
      segments.addAll(forme.dessiner());
    }
    return segments;
  }

  public abstract String typeForme();

  public String getNom() {
    return nom;
  }

  public void setNom(String nom) {
    this.nom = nom;
  }

  public ArrayList<Forme> getFormesComposees() {
    return formesComposees;
  }

  public void setFormesComposees(ArrayList<Forme> formesComposees) {
    this.formesComposees = formesComposees;
  }

  public String toString() {
    return "FormeCompose [nom=" + nom + ", formesComposees=" + formesComposees + "]";
  }

}
