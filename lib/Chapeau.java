import java.util.ArrayList;
import ardoise.*;

public class Chapeau extends Forme {

    private PointPlan p1;
    private PointPlan p2;
    private PointPlan p3;
    
    public Chapeau(String nom, PointPlan p1, PointPlan p2, PointPlan p3){
        super(nom);
        this.p1 = p1;
        this.p2 = p2;
        this.p3 = p3;
    }

    public Chapeau(Chapeau c){
        this.p1 = c.p1;
        this.p2 = c.p2;
        this.p3 = c.p3;
    }

    public void deplacer(int deplacementX, int deplacementY) {
        p1.setAbscisse(p1.getAbscisse() + deplacementX);
        p1.setOrdonnee(p1.getOrdonnee() + deplacementY);
        p2.setAbscisse(p2.getAbscisse() + deplacementX);
        p2.setOrdonnee(p2.getOrdonnee() + deplacementY);
        p3.setAbscisse(p3.getAbscisse() + deplacementX);
        p3.setOrdonnee(p3.getOrdonnee() + deplacementY);
    }
    

    public ArrayList<Segment> dessiner() {
        ArrayList<Segment> segments = new ArrayList<>();
        segments.add(new Segment(p1, p2));
        segments.add(new Segment(p2, p3));
        return segments;
    }

    public String typeForme() {
        return "C";
    }

    public PointPlan getP1() {
        return p1;
    }

    public void setP1(PointPlan p1) {
        this.p1 = p1;
    }

    public PointPlan getP2() {
        return p2;
    }

    public void setP2(PointPlan p2) {
        this.p2 = p2;
    }

    public PointPlan getP3() {
        return p3;
    }

    public void setP3(PointPlan p3) {
        this.p3 = p3;
    }

    public String toString() {
        return "Chapeau [p1=" + p1 + ", p2=" + p2 + ", p3=" + p3 + "]";
    }
} 