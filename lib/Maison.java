import java.util.ArrayList;
import ardoise.*;

public class Maison extends FormeCompose {

    public String nom;
    private Chapeau toit;
    private Quadrilatere corps;
    private Quadrilatere porte;

    public Maison(String nom, Quadrilatere corps, Chapeau toit, Quadrilatere porte) {
        super(nom);
        this.toit = toit;
        this.corps = corps;
        this.porte = porte;
    }

    public Maison(Maison m) {
        super(m.nom);
        this.toit = m.toit;
        this.corps = m.corps;
        this.porte = m.porte;
    }

    public void deplacer(int deplacementX, int deplacementY) {
        toit.deplacer(deplacementX, deplacementY);
        corps.deplacer(deplacementX, deplacementY);
        porte.deplacer(deplacementX, deplacementY);

    }

    public ArrayList<Segment> dessiner() {
        ArrayList<Segment> segments = new ArrayList<>();
        segments.addAll(toit.dessiner());
        segments.addAll(corps.dessiner());
        segments.addAll(porte.dessiner());
        return segments;
    }

    public String typeForme() {
        return "GF";
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Chapeau getToit() {
        return toit;
    }

    public void setToit(Chapeau toit) {
        this.toit = toit;
    }

    public Quadrilatere getCorps() {
        return corps;
    }

    public void setCorps(Quadrilatere corps) {
        this.corps = corps;
    }

    public Quadrilatere getPorte() {
        return porte;
    }

    public void setPorte(Quadrilatere porte) {
        this.porte = porte;
    }

    public String toString() {
        return "Maison [nom=" + nom + ", toit=" + toit + ", corps=" + corps + ", porte=" + porte + "]";
    }
}
